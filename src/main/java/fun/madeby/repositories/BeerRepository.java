package fun.madeby.repositories;

import fun.madeby.models.Beer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BeerRepository extends JpaRepository<Beer, Integer> {

}

package fun.madeby.repositories;

import fun.madeby.models.Brewer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrewerRepository extends JpaRepository<Brewer, Integer> {
}

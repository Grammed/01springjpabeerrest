package fun.madeby.controllers;


import fun.madeby.models.Brewer;
import fun.madeby.services.BrewerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/v1")
public class BrewerController {
	@Autowired
	BrewerService brewerService;

	public BrewerController(BrewerService brewerService){
		this.brewerService = brewerService;
	}

	@GetMapping("/brewers")
public ResponseEntity<List<Brewer>> get() {
		List<Brewer> brewers = brewerService.findAll();
		return new ResponseEntity<>(brewers, HttpStatus.OK);
	}
}

package fun.madeby.controllers;


import fun.madeby.models.Beer;
import fun.madeby.services.BeerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/v1")
public class BeerController {
//@Autowired
BeerService beerService;

public BeerController(BeerService beerService) {
	this.beerService = beerService;
}

@GetMapping("/beers")
public ResponseEntity<List<Beer>> get() {
	List<Beer> beers = beerService.findAll();
	return new ResponseEntity<>(beers, HttpStatus.OK);
}

@PostMapping("/beers")
public ResponseEntity<Beer> save(@RequestBody Beer beer) {
	Beer beerReturned = beerService.save(beer);
	return new ResponseEntity<>(beerReturned, HttpStatus.OK);
}



}

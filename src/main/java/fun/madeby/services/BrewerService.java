package fun.madeby.services;

import fun.madeby.models.Brewer;
import java.util.List;

public interface BrewerService {
List<Brewer> findAll();
Brewer save(Brewer brewer);
Brewer findById(Integer id);
void delete(Integer id);
}

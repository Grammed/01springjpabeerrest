package fun.madeby.services;

import fun.madeby.models.Brewer;
import fun.madeby.repositories.BrewerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrewerServiceImpl implements BrewerService{

	@Autowired
	private BrewerRepository brewerRepository;


@Override
public List<Brewer> findAll() {
	return brewerRepository.findAll();
}

@Override
public Brewer save(Brewer brewer) {
	brewerRepository.save(brewer);
	return brewer;
}

@Override
public Brewer findById(Integer id) {
	if(brewerRepository.findById(id).isPresent())
		return brewerRepository.findById(id).get();
	return null;
}

@Override
public void delete(Integer id) {
	Brewer brewer = findById(id);
	brewerRepository.delete(brewer);

}
}

package fun.madeby.services;

import fun.madeby.models.Beer;
import java.util.List;

public interface BeerService {
	List<Beer> findAll();
	Beer save(Beer beer);
	Beer findById(Integer id);
	void delete(Integer id);
}

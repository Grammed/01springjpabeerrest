package fun.madeby.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "brewers")
public class Brewer  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Address")
    private String address;
    @Column(name = "ZipCode")
    private String zipCode;
    @Column(name = "City")
    private String city;
    @Column(name = "Turnover")
    private int turnover;
    @OneToMany(mappedBy = "brewer", cascade = {CascadeType.PERSIST, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
    private Set<Beer> beers = new HashSet<>();
    public Brewer() {
    }
    public Brewer(String name, String address, String zipCode, String city, int turnover) {
        this(name, address, zipCode, city, turnover, new HashSet<>());
    }
    public Brewer(String name, String address, String zipCode, String city, int turnover, Set<Beer> beers) {
        setName(name);
        setAddress(address);
        setZipCode(zipCode);
        setCity(city);
        setTurnover(turnover);
        setBeers(beers);
    }
    public long getId() {
        return id;
    }
    private void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public int getTurnover() {
        return turnover;
    }
    public void setTurnover(int turnover) {
        this.turnover = turnover;
    }
    public Set<Beer> getBeers() {
        return beers;
    }
    public void setBeers(Set<Beer> beers) {
        this.beers = beers;
    }

@Override
public String toString() {
    return "Brewer{" +
              "id=" + id +
              ", name='" + name + '\'' +
              ", address='" + address + '\'' +
              ", zipCode='" + zipCode + '\'' +
              ", city='" + city + '\'' +
              ", turnover=" + turnover +
              '}';
}

@Override
public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Brewer brewer = (Brewer) o;
    return id == brewer.id;
}

@Override
public int hashCode() {
    return Objects.hash(id);
}
}

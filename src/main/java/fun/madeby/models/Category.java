package fun.madeby.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "categories")
public class Category {
    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "Category")
    private String name;
    @OneToMany(mappedBy = "category")
    private Set<Beer> beers = new HashSet<Beer>();

    public Category() {
    }

    public Category(String name) {
        this(name, new HashSet<Beer>());
    }
    public Category(String name, Set<Beer> beers) {
        setName(name);
        setBeers(beers);
    }

    public long getId() {
        return id;
    }
    private void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Set<Beer> getBeers() {
        return beers;
    }
    public void setBeers(Set<Beer> beers) {
        this.beers = beers;
    }

@Override
public String toString() {
    return "Category{" +
              "id=" + id +
              ", name='" + name + '\'' +
              '}';
}

@Override
public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Category category = (Category) o;
    return id == category.id;
}

@Override
public int hashCode() {
    return Objects.hash(id);
}
}
